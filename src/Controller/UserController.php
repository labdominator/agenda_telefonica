<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/", name="user_index", methods={"GET"})
     */
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('user/index.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="user_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_show", methods={"GET"})
     */
    public function show(User $user): Response
    {
        return $this->render('user/show.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"DELETE"})
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_index');
    }

    
    /**
     * @Route("/login", name="login")
     */
    public function login(Request $request, UserRepository $userRepository)
    {
    	

    	// if(isset($_POST['nome'])) {
    	// 	$nome = $_POST['nome'];
    	// 	$pwd = $_POST['nome'];
    	// 	/*$nome = $request->get('nome');
    	// 	$pwd = $request->get('senha');*/


	    // 	$users = $userRepository->findAll();

	    // 	foreach ($users as $user) {
	    // 		if($user->getNome() == $nome) {
	    // 			if ($user->getSenha() == $pwd) {
	    // 				return $this->render('default/index.html.twig', [
	    //         			'user' => $user,
	    //         			'erro' => false
	    //     			]);
	    // 			}else{
	    // 				return $this->render('default/index.html.twig', [
		//     				'msg'=> "Senha errada nn",
		//     				'erro'=> true
		//     			]);
	    // 			}
	    // 		}else{
	    // 			return $this->render('default/index.html.twig', [
	    // 				'msg'=> "Nome errado",
	    // 				'erro'=> true
	    // 			]);
	    // 		}
	    // 	}
	    // }else {
		// 	return $this->render('default/formLogin.html.twig');
		// }

    }
}
