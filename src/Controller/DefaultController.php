<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/d")
 */
class DefaultController extends AbstractController
{

    /**
     * @Route("/", name="home", methods={"GET"})
     */
    public function index()
    {
		return $this->render('default/index.html.twig',[
			'msg' => "Mendes Apolo",
			'erro' => true
		]);
	}
}
